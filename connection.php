<?php

class DatabaseConnector
{
    var $db;             
    var $dbHost;     
    var $dbUsername; 
    var $dbPassword; 
    var $dbConnection;
    
    public function __construct($db, $host, $username, $password)
    {
        $this->db           = $db;
        $this->dbHost       = $host;
        $this->dbUsername   = $username;
        $this->dbPassword   = $password;
        $this->dbConnection = NULL;
     
        $this->connectToDatabase();
    }
    
    public function __destruct()
    {   
        $this->closeConnection();
        
        $this->db           = NULL;
        $this->dbHost       = NULL;
        $this->dbUsername   = NULL;
        $this->dbPassword   = NULL;
        $this->dbConnection = NULL;
        
        echo "Database object destroyed.\n";
    }
    
    public function executeQuery($query)
    {
        mysql_select_db($this->db);
    
        if(mysql_error())
        {
            die("Cannot find database:".$this->db);
    
            return;
        }
    
        echo "Database:".$this->db." selected.\n";
    
        $result = mysql_query($query);
    
        if($result == TRUE)
        {
            echo "Query executed successfully.\n";
        }
        else
        {
            echo "Query failed.\n";
        }
    
        return $result;
    }
    
    private function connectToDatabase()
    {
        $connection = mysql_connect($this->dbHost, $this->dbUsername, $this->dbPassword, $this->db);

        if (!$connection)
        {
            die("Cannot connect to database: ".$this->db."\n");
        }
        
        $this->dbConnection = $connection;

        echo "Connection established.\n";
    }
        
    private function closeConnection()
    {
        mysql_close($this->dbConnection);
        
        echo "Database connection closed.\n";
    }
}
    
?>